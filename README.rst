docker-polarbytebot-gw2-developername-updater
=============================================

* all actions to the database and all the actual logic is part of the `polarbytebot library <https://gitlab.com/networkjanitor/libpolarbytebot>`_
* this is a small microservice/dockercontainer which updates the names of flaired arenanet developers from /r/GuildWars2's stylesheet